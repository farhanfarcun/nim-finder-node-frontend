import React from 'react';
import './App.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Input } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';

const barstyle = {
  background: '#2763d8'
}

const inpstyle = {
  width: '300px',
}

const tablestyle = {
  width: '50%',
  marginTop: 25,
  overflowX: 'auto',
};

const bodstyle = {
  marginLeft: 20
}

const butstyle = {
  marginTop: 20,
  marginBottom: 20,
  marginLeft: 110
}

class App extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      query : '',
      page : 0,
      count : 10,
      data : [],
      isAny : undefined
    }
    this.handleChange = this.handleChange.bind(this);
  }

  setQuery = (event, newq) => {
    this.setState({
      query: newq
    })
  }

  setCount = (event, newc) => {
    this.setState({
      count: newc
    })
  }

      /* Fungsi incrPage()
       Digunakan untuk melakukan increment pada state page
       Menggunakan versi setState() dengan pemanggilan fungsi
    */
   incrPage = () => {
    this.setState((prevState) => {
        return {page: prevState.page + 1}
        })
    }

    /* Fungsi decrPage()
      Digunakan untuk melakukan decrement pada state page
      Menggunakan versi setState() dengan pemanggilan fungsi
    */
    decrPage = () => {
        this.setState((state) => {
            if (state.page - 1 >= 0) {
                return {page: state.page - 1}
            }
        })
    }

    /* Fungsi neutPage()
      Digunakan untuk set page dengan nilai 0
      Menggunakan versi setState() dengan pemanggilan fungsi
    */
    neutPage = () => {
        this.setState((state) => {
            return {page: 0}
        })
    }

    /* Fungsi handleNext
       @param = event
       Melakukan pengiriman data ke API ketika ada event oleh nextButton
    */
  handleNext = (event) => {
    var url = 'https://nim-finder-itb-farcun.herokuapp.com/nim';
    if (this.state.query.length !== 0) {
        this.incrPage()
        axios.post(url,{
          query: this.state.query,
          page: this.state.page+1
        }).then(
            response => this.handleNextResponse(response)
        )
    }
  }

  /* Fungsi handlePrev
    @param = event
    Melakukan pengiriman data ke API ketika ada event oleh prevButton
  */
  handlePrev = (event) => {
    var url = 'https://nim-finder-itb-farcun.herokuapp.com/nim';
    if (this.state.query.length !== 0) {
      if(this.state.page > 0) {
        this.decrPage()
        axios.post(url,{
          query: this.state.query,
          page: this.state.page-1
        }).then(
            response => this.handleResponse(response)
        )
      }
    }
  }
  
    /* Fungsi handleNextResponse
       @param = response
       Melakukan validasi pada response yang diterima dari API
       ketika ada event oleh NextButton
    */
  handleNextResponse = (response) => {
    if (response.data.payload.length !== 0) {
        this.handleResponse(response)
    } else {
        this.decrPage()
    }
  }

  handleChange = (event) => {
    var url = 'https://nim-finder-itb-farcun.herokuapp.com/nim';
    this.neutPage();
    this.setState({
      query: event.target.value
    });
    axios.post(url,{
      query: event.target.value
    }).then(
      response => this.handleResponse(response)
    )
  }

  handleResponse = (response) => { 
      if (response.data.code === 0) {
          this.setState({
              isAny: false
          })
      } else if (response.data.code > 0) {
          this.setState({
              data: response.data.payload,
              isAny: true
          })
      }
    }

  renderUtil = () => {
      if (this.state.isAny === true ) {
          return (
              <div className='body' style={bodstyle}>
                <div className='table'>
                    <Table style={tablestyle} >
                        <TableHead>
                            <TableRow>
                                <TableCell style={{fontSize: '13pt'}}>Nama</TableCell>
                                <TableCell style={{fontSize: '13pt'}}>NIM TPB</TableCell>
                                <TableCell style={{fontSize: '13pt'}}>NIM Jurusan</TableCell>
                                <TableCell style={{fontSize: '13pt'}}>Jurusan</TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {
                                this.state.data.map(function(res) {
                                  var jurusan = ''
                                  if (res.studentMajorID !== undefined) {
                                    var kodeJur = res.studentMajorID.substring(0, 3);
                                    switch(kodeJur) {
                                      case '101' :
                                        jurusan = 'Matematika'
                                        break
                                      case '102' :
                                        jurusan = 'Fisika'
                                        break
                                      case '103' :
                                        jurusan = 'Astronomi'
                                        break
                                      case '104' :
                                        jurusan = 'Mikrobiologi'
                                        break
                                      case '105' :
                                        jurusan = 'Kimia'
                                        break
                                      case '106' :
                                        jurusan = 'Biologi'
                                        break
                                      case '107' :
                                        jurusan = 'Sains dan Teknologi Farmasi'
                                        break
                                      case '108' :
                                        jurusan = 'Aktuaria'
                                        break
                                      case '112' :
                                        jurusan = 'Rekayasa Hayati'
                                        break
                                      case '114' :
                                        jurusan = 'Rekayasa Pertanian'
                                        break
                                      case '115' :
                                        jurusan = 'Rekayasa Kehutanan'
                                        break
                                      case '116' :
                                        jurusan = 'Farmasi Klinik dan Komunitas'
                                        break
                                      case '119' :
                                        jurusan = 'Teknologi Pascapanen'
                                        break
                                      case '120' :
                                        jurusan = 'Teknik Geologi'
                                        break
                                      case '121' :
                                        jurusan = 'Teknik Pertambangan'
                                        break
                                      case '122' :
                                        jurusan = 'Teknik Perminyakan'
                                        break
                                      case '123' :
                                        jurusan = 'Teknik Geofisika'
                                        break
                                      case '124' :
                                        jurusan = 'Geofisika'
                                        break
                                      case '125' :
                                        jurusan = 'Teknik Metalurgi'
                                        break
                                      case '128' :
                                        jurusan = 'Meteorologi'
                                        break
                                      case '129' :
                                        jurusan = 'Oseanografi'
                                        break
                                      case '130' :
                                        jurusan = 'Teknik Kimia'
                                        break
                                      case '131' :
                                        jurusan = 'Teknik Mesin'
                                        break
                                      case '132' :
                                        jurusan = 'Teknik Elektro'
                                        break
                                      case '133' :
                                        jurusan = 'Fisika Teknik'
                                        break
                                      case '134' :
                                        jurusan = 'Teknik Industri'
                                        break
                                      case '135' :
                                        jurusan = 'Teknik Informatika'
                                        break
                                      case '136' :
                                        jurusan = 'Teknik Dirgantara'
                                        break
                                      case '137' :
                                        jurusan = 'Teknik Material'
                                        break
                                      case '143' :
                                        jurusan = 'Teknik Pangan'
                                        break
                                      case '144' :
                                        jurusan = 'Manajemen Rekayasa Industri'
                                        break
                                      case '145' :
                                        jurusan = 'Teknik Bioenergi dan Kemurgi'
                                        break
                                      case '150' :
                                        jurusan = 'Teknik Sipil'
                                        break
                                      case '151' :
                                        jurusan = 'Teknik Geodesi dan Geomatika'
                                        break
                                      case '152' :
                                        jurusan = 'Arsitektur'
                                        break
                                      case '153' :
                                        jurusan = 'Teknik Lingkungan'
                                        break
                                      case '154' :
                                        jurusan = 'Perencanaan Wilayah dan Kota'
                                        break
                                      case '155' :
                                        jurusan = 'Teknik Kelautan'
                                        break
                                      case '157' :
                                        jurusan = 'Rekayasa Infrastruktur Lingkungan'
                                        break
                                      case '158' :
                                        jurusan = 'Teknik dan Pengelolaan Sumber Daya Air'
                                        break
                                      case '170' :
                                        jurusan = 'Seni Rupa'
                                        break
                                      case '172' :
                                        jurusan = 'Kriya'
                                        break
                                      case '173' :
                                        jurusan = 'Desain Interior'
                                        break
                                      case '174' :
                                        jurusan = 'Desain Komunikasi Visual'
                                        break
                                      case '175' :
                                        jurusan = 'Desain Produk'
                                        break
                                      case '180' :
                                        jurusan = 'Teknik Tenaga Listrik'
                                        break
                                      case '181' :
                                        jurusan = 'Teknik Telekomunikasi'
                                        break
                                      case '182' :
                                        jurusan = 'Sistem dan Teknologi Informasi'
                                        break
                                      case '183' :
                                        jurusan = 'Teknik Biomedis'
                                        break
                                      case '190' :
                                        jurusan = 'Manajemen'
                                        break
                                      case '192' :
                                        jurusan = 'Kewirausahaan'
                                        break
                                      default:
                                        jurusan = '';
                                        break
                                    } 
                                  }
                                    return (
                                        <TableRow key={res.studentID}>
                                            <TableCell>{res.name}</TableCell>
                                            <TableCell>{res.studentID}</TableCell>
                                            <TableCell>{res.studentMajorID}</TableCell>
                                            <TableCell>{jurusan}</TableCell>
                                        </TableRow>
                                    )
                                })
                            }
                        </TableBody>
                    </Table>

                    <MuiThemeProvider>
                      <div className='Button' style={butstyle}>
                        <RaisedButton
                        label='Prev'
                        onClick={this.handlePrev}>
                        </RaisedButton>

                        <RaisedButton
                        label='Next'
                        onClick={this.handleNext}>
                        </RaisedButton>
                      </div>
                    </MuiThemeProvider>
                </div>
              </div>
          )
      } else if (this.state.isAny === false) {
          return (
              <Table style={tablestyle} >
                  <TableHead>
                      <TableRow>
                          <TableCell>
                              Data tidak ditemukan
                          </TableCell>
                      </TableRow>
                  </TableHead>
              </Table>
          )
      }
  }

  mainRender = () => {
    if(this.state.query !== '') {
      return this.renderUtil();
    }
  }

  render() {
    return (
      <div className='App'>
          <div className='Header'>
            <AppBar style={barstyle}>
              <Toolbar>
                <div className='Head'>
                  <Typography variant='h5' color='inherit'>
                    NIM Finder ITB Ulala
                  </Typography>
                </div>
              </Toolbar>
            </AppBar>
          </div>
  
          <div className='Body'>
            <Input
            placeholder='Find Name/NIM'
            onChange={this.handleChange}
            style={inpstyle}>
            </Input>
          </div>
  
          <div className='Footer'>
  
          </div>
            {this.mainRender()}
      </div>
    );
  }
}

export default App;
